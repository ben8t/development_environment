# Development environment

## Python environment

### Build command

`docker build -t pydock .`

### Running commands

* Bash: `docker container run --rm -it -v $(pwd):/data --name ipython pydock bash`

* IPython: `docker container run --rm -it -v $(pwd):/data --name ipython pydock ipython`

* Jupyter Notebook: `docker container run -p 8888:8888 -v $(pwd):/data --name jupyter pydock jupyter notebook --notebook-dir=/data --ip='*' --port=8888 --no-browser --allow-root`

* Jupyter Lab: alias `docker container run --rm -p 8888:8888 -v $(pwd):/data --name jupyter pydock jupyter lab --notebook-dir=/data --ip="*" --port=8888 --no-browser --allow-root`

## R & RStudio environment

### Build command

`docker build -t my_r .`

### Running commands

* RStudio in browser : `docker run -d --rm -v $(pwd):/home/rstudio/kitematic -p 8787:8787 -e USER=ben -e PASSWORD=root --name rstudio my_r`
login: ben
password: root

## Sources

* https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image
* https://hub.docker.com/r/ufoym/deepo/
